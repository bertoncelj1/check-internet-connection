#!/bin/bash


SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"
result=$($SCRIPTPATH/check_connection.sh)

echo "$(date "+%Y-%m-%d %H:%M:%S") $result" >> "$SCRIPTPATH/connection.log"
