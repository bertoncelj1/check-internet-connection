#!/bin/bash

# empty -> false, 
# not empty -> true
DEBUG="" 

debug() {
    if ! [[ $DEBUG ]]; then return; fi
    
    echo $@
}

printGateway() {
 /sbin/route -n | sed -n "/^0.0.0.0/s/^0.0.0.0[^0-9]*\([0-9][^ ]*\) .*/\1/p";
}

printLocalIps() {
 /urs/sbin/arp | cut -d" " -f1 | tail -n +2 | tr "\n" " "
}

logError() {
    # output debug message 
    debug $@

    if ! [[ $LOGERR ]]; then return; fi
    
    echo $(date -u) $@ >> $errLogFile
}

# checkConnection $url 
#
# check connection by using wget
chekcConnection() {
    url="$1"    
    
    wget -q --spider $url

    hasConnection=$?
    
    if [ $hasConnection -eq 0 ]; then
        debug "OK connection to website: " $url
    else
        logError "NO connection to website: " $url
    fi
    
    return $hasConnection
}

# checkConnectionPing $url
#
# ping given url
checkConnectionPing() {
    hostName="$1"

    debug "Pinging $hostName"

    ping -c 1 $hostName &> /dev/null;
    
    hasConnection=$?
    
    if [ $hasConnection -eq 0 ]; then
        debug "ping OK: $hostName"
    else
        debug "ping ERROR $hasConnection: $hostName"
    fi
    
    return $hasConnection
}


# testConnectionToSites site1 site2 site3 ...
#
# using ping check connection to all provided sites than print result on
# a single line
# returns 0 (true) if atleast one connection was successful 
testConnectionToSites() {
  siteArray="$1";
  
  atLeastOne="";
  
  for site in ${siteArray[*]}; do
      printf "$site "
      checkConnectionPing $site
      connectionStatus=$?
      
      if [ "$connectionStatus" -eq 0 ]; then
        statusStr="OK"
        atLeastOne=true
      else
        statusStr="ERR$connectionStatus"
      fi
      
      printf "$statusStr "
    done
    
  if [ $atLeastOne ]; then return 0; else return 1; fi
}

# checkConnectionInternet <no arguments>
#
# check connection to the internet by first pinging well known websites
# and also pinging local ips and gateway
checkConnectionInternet() {
    remote="stackoverflow.com yahoo.com twitter.com google.com"
    local="$(printGateway)" #$(printLocalIps) # checking againts local IPs can be very slow
    
    debug "Checking remote connection"
    testConnectionToSites "$remote"
    remoteResult=$?;
    
    debug "Chcking local connctions"
    testConnectionToSites "$local"
    localResult=$?
    
    echo " Remote $remoteResult Local $localResult "
}

checkConnectionInternet

